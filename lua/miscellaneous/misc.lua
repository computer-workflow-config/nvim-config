-- Confirm completion item if popup menu is visible, otherwise insert a literal tab
--vim.api.nvim_set_keymap('i', '<Tab>', 'pumvisible() ? coc#_select_confirm() : "<C-g>u<Tab>"', { expr = true, silent = true })

-- Undo the last change and move the cursor to the start of the line
--vim.api.nvim_set_keymap('i', '<CR>', '"<C-g>u<CR>"', { expr = true, silent = true })
vim.api.nvim_set_keymap('t', '<Esc>', '<C-\\><C-n>', {noremap = true})

-- Change leader key to space
vim.g.mapleader = " " 
